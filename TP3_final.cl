;;===================== BASE DE QUESTIONS ==========================================
(setq BQ 
      '(
        (temps "Connaissez vous votre temps disponibles par jour ? (0 <= 30 min, 1 <= 1h, 2 <= 2h, 3 > 2h)")
        (frequence_semaine "Connaissez vos nombres de jours disponibles par semaine ?")
        (halteres "Avez vous des halt�res ? True - False")
        (corde_saute "Avez vous une corde � sauter ? True - False")
        (barre_traction "Avez vous une barre de traction ? True - False")
        (acces_sds "Avez vous acc�s � une salle de sport ? True - False")
        (niveau "Connaisez vous votre niveau de sport ? 0 = debutant 1 = Intermediaire 2 = Confirm� 3 = Expert")
        (delta_poids "Combien de kilos souhaitez-vous perdre ou gagner ? ")
        )
      )


;;==================== BASE DE REGLES ======================================================
(setq *BR* '(
             (RO (nil) (nil))
             
             ;;===========REGLES LIEES A LA FREQUENCE===========
             
             (R1 ((>= frequence_semaine 0) (< frequence_semaine 2)) (eq frequence_reel 0))
             (R2 ((>= frequence_semaine 2) (< frequence_semaine 4)) (eq frequence_reel 1))
             (R3 ((>= frequence_semaine 4)) (eq frequence_reel 2))
             
             
             ;;===========REGLES LIEES AU TEMPS===========
             
             (R4 ((>= temps 0) (< temps 1)) (eq temps_reel 0))
             (R5 ((>= temps 1) (< temps 3)) (eq temps_reel 1))
             (R6 ((>= temps 3)) (eq temps_reel 2))
             
                      
             ;;===========REGLES LIEES AU DELTA POIDS POUR OBTENIR L'OBJECTIF===========
             
             (R7 ((>= delta_poids 15)) (eq objectif perte_poids))
             (R8 ((<= delta_poids -5)) (eq objectif prise_muscle))
             
             (R9 ((>= delta_poids -5) (< delta_poids 5)) (eq objectif sport_preventif))
             (R10 ((>= delta_poids -5) (< delta_poids 5)) (eq objectif amelioration_capacite_athletique))
             (R11 ((>= delta_poids -3) (< delta_poids 15)) (eq objectif rester_en_forme))
             
             ;;===========REGLES LIEES AU MATERIEL===========
               
             (R12 ((eq halteres false) (eq corde_saute false) (eq barre_traction false)) (eq homegym 0))
             (R13 ((eq halteres true) (eq corde_saute true) (eq barre_traction true)) (eq homegym 2))
             (R14 ((eq halteres true) (eq corde_saute true) (eq barre_traction false)) (eq homegym 2))
             (R15 ((eq halteres true) (eq corde_saute false) (eq barre_traction true)) (eq homegym 2))
             (R16 ((eq halteres false) (eq corde_saute true) (eq barre_traction true)) (eq homegym 2))
             (R17 ((eq halteres true) (eq corde_saute false) (eq barre_traction false)) (eq homegym 1))
             (R18 ((eq halteres false) (eq corde_saute true) (eq barre_traction false)) (eq homegym 1))
             (R19 ((eq halteres false) (eq corde_saute false) (eq barre_traction true)) (eq homegym 1))
             
             (R20 ((eq acces_sds true)) (eq materiel 2))
             (R21 ((eq homegym 2)) (eq materiel 2))
             (R22 ((eq homegym 1)) (eq materiel 1))
             (R23 ((eq homegym 0)) (eq materiel 0))
             
                         
             ;;===========REGLES LIEES A LA DUREE===========
             
             (R24 ((eq frequence_reel 0) (eq temps_reel 0)) (eq duree faible))
             (R25 ((eq frequence_reel 0) (eq temps_reel 1)) (eq duree faible))
             (R26 ((eq frequence_reel 1) (eq temps_reel 0)) (eq duree faible))
             (R27 ((eq frequence_reel 1) (eq temps_reel 1)) (eq duree moderee))
             (R28 ((eq frequence_reel 1) (eq temps_reel 2)) (eq duree moderee))
             (R29 ((eq frequence_reel 2) (eq temps_reel 1)) (eq duree moderee))
             (R30 ((eq frequence_reel 2) (eq temps_reel 2)) (eq duree importante))
             
             
             ;;===========REGLES LIEES A LA VOLONTE===========
             
             (R29 ((eq duree moderee) (eq materiel 2)) (eq volonte forte))
             (R30 ((eq duree importante) (eq materiel 2)) (eq volonte forte))
             
             (R31 ((eq duree moderee) (>= materiel 1)) (eq volonte moyen))
             (R32 ((eq duree importante) (>= materiel 1)) (eq volonte moyen))
             
             (R33 ((eq duree moderee) (eq materiel 0)) (eq volonte faible))
             (R34 ((eq duree faible) (eq materiel 0)) (eq volonte faible))
             (R35 ((eq duree faible) (eq materiel 1)) (eq volonte faible))
             (R36 ((eq duree faible) (eq materiel 2)) (eq volonte faible))
             
             
             ;;===========REGLES LIEES A L'ENTRAINEMENT MUSCULATION===========
             
             (R37 ((eq volonte forte) (eq objectif prise_muscle)) (eq entrainement_temp musculation))
             (R38 ((eq volonte moyen) (eq objectif prise_muscle)) (eq entrainement_temp musculation))
             (R39 ((eq volonte forte) (eq objectif rester_en_forme)) (eq entrainement_temp musculation))
             (R40 ((eq volonte moyen) (eq objectif rester_en_forme)) (eq entrainement_temp musculation))

                          
             (R41 ((eq entrainement_temp musculation) (eq acces_sds true)) (eq entrainement musculation_en_salle))
             (R42 ((eq entrainement_temp musculation) (>= materiel 1)) (eq entrainement musculation_maison))
             
             
             ;;===========REGLES LIEES A L'ENTRAINEMENT CARDIO===========
                       
             (R43 ((eq volonte moyen) (eq objectif rester_en_forme)) (eq entrainement cardio))
             (R44 ((eq volonte forte) (eq objectif rester_en_forme)) (eq entrainement cardio))
             (R45 ((>= materiel 0) (eq objectif rester_en_forme)) (eq entrainement cardio))

             (R46 ((eq volonte moyen) (eq objectif amelioration_capacite_athletique)) (eq entrainement cardio))
             (R47 ((eq volonte forte) (eq objectif amelioration_capacite_athletique)) (eq entrainement cardio))
             (R48 ((>= materiel 0) (eq objectif amelioration_capacite_athletique)) (eq entrainement cardio))

             
             ;;===========REGLES LIEES A L'ENTRAINEMENT Crossfit===========
             
             (R49 ((eq volonte moyen) (eq objectif perte_poids) (>= niveau 2)) (eq entrainement crossfit))
             (R50 ((eq volonte forte) (eq objectif perte_poids) (>= niveau 2)) (eq entrainement crossfit))

             (R51 ((eq volonte moyen) (eq objectif amelioration_capacite_athletique) (>= niveau 2)) (eq entrainement crossfit))
             (R52 ((eq volonte forte) (eq objectif amelioration_capacite_athletique) (>= niveau 2)) (eq entrainement crossfit))

             (R53 ((eq volonte moyen) (eq objectif prise_muscle)  (>= niveau 2))(eq entrainement crossfit))
             (R54 ((eq volonte forte) (eq objectif prise_muscle)  (>= niveau 2))(eq entrainement crossfit))
             
             
             ;;===========REGLES LIEES A L'ENTRAINEMENT TABATA===========

             (R55 ((eq volonte faible) (eq objectif perte_poids)) (eq entrainement tabata))
             (R56 ((eq volonte moyen) (eq objectif perte_poids)) (eq entrainement tabata))

             (R57 ((eq volonte faible) (eq objectif rester_en_forme)) (eq entrainement tabata))
             (R58 ((eq volonte moyen) (eq objectif rester_en_forme)) (eq entrainement tabata))
             
                ;;=====Gestion d'un cas particulier pour colelr un peu plus au reel===== 
             (R59 ((eq duree moderee) (eq volonte moyen) (eq objectif rester_en_forme)) (eq entrainement tabata))
             (R60 ((eq duree moderee) (eq volonte moyen) (eq perte_poids)) (eq entrainement tabata))
             
             (R61 ((eq duree moderee) (eq volonte forte) (eq objectif rester_en_forme)) (eq entrainement tabata))
             (R62 ((eq duree moderee) (eq volonte forte) (eq perte_poids)) (eq entrainement tabata))
             
             
             ;;===========REGLES LIEES A L'ENTRAINEMENT REMISE_EN_FORME===========
             
             (R63 ((eq volonte moyen) (eq objectif perte_poids) (<= niveau 1)) (eq entrainement remise_en_forme))
             (R64 ((eq volonte forte) (eq objectif perte_poids) (<= niveau 1)) (eq entrainement remise_en_forme))

             (R65 ((eq volonte moyen) (eq objectif rester_en_forme) (<= niveau 1)) (eq entrainement remise_en_forme))
             (R66 ((eq volonte forte) (eq objectif rester_en_forme) (<= niveau 1)) (eq entrainement remise_en_forme))

             (R67 ((eq volonte moyen) (eq objectif sport_preventif) (<= niveau 1)) (eq entrainement remise_en_forme))
             (R68 ((eq volonte forte) (eq objectif sport_preventif) (<= niveau 1)) (eq entrainement remise_en_forme))
             
             
             
             
             
             ;;===========AJOUT DE REGLES POUR ETOFFER LE SE===========
             
             (R69 ((eq objectif perte_poids)(eq duree importante)) (eq chargeDeTravail importante))
             (R70 ((eq chargeDeTravail importante)) (eq entrainement insanity))
             
             (R71 ((eq objectif perte_poids)(eq duree moderee))(eq chargeDeTravail moderee))
             (R72 ((eq chargeDeTravail moderee)(eq acces_sds true))(eq entrainement CardioSalle))
             (R73 ((eq chargeDeTravail importante)(eq acces_sds true))(eq entrainement CardioSalle))
             
             (R74 ((eq objectif sport_preventif)(>= temps 1))(eq type RenfoMusculaire))
             (R75 ((eq type RenfoMusculaire)(>= materiel 1))(eq entrainement InsaneAbs))
             (R76 ((eq objectif sport_preventif))(eq entrainement etirements))
             
             (R76 ((eq objectif rester_en_forme)(>= temps 1)(>= materiel 0))(eq entrainement GainageAbs))
             
             ))



;;======================= FONCTIONS ASSOCIEES AUX REGLES ===============================

;;Fonction qui retourne la conclusion d'une regle 
(defun termes-conclusion (regles)
  (caddr regles)
  )

;;Fonction qui retourne les pr�misses d'une r�gle 
(defun termes-conditions (regles)
  (cadr regles)
  )

;;Fonction qui retourne le num�ro de la r�gle
(defun numRegle (regle)
  (car regle)
  )

;;Fonction renvoyant vrai si le fait (op�rateur condition valeur) appartient � la base de fait, faux sinon.
(defun connu? (fait baseF)
  (let((valeur (cadr (assoc (cadr fait) baseF))))
    (if valeur
        (funcall (car fait) valeur (caddr fait)))))

;;Fonction renvoyant une liste contenant toutes les r�gles permettant d'obtenir le but en question
(defun regles-candidates (but bdR)
  (if (null bdR) ()
    (let* (
            (conclusion (termes-conclusion (car bdR)))
            (attribut (cadr conclusion))
            (valeur (caddr conclusion))
           )
      (if (and (eq attribut (cadr but)) (funcall (car but) valeur (caddr but)))
          (cons (car bdR) (regles-candidates but (cdr bdR)))
        (regles-candidates but (cdr bdR))))))

;;Fonction qui renvoie vrai si le but correspond � au moins une conclusion d'une r�gle
(defun conclusion? (but BR)
  (dolist (regles BR nil)
    (if (equal but (termes-conclusion regles))
        (return-from conclusion? T))))

;;-------- FONCTIONS MAIN --------------------

;;Fonction permettant de calculer la difference entre le poids donn� par l'utilisateur et son poids recherch�e
(defun ObjectifPoids()
  (let(
       (poids nil)
       (diff nil) 
       (objectif nil)
       )
    (format t "~%Quel est votre poids ?~%")
    (setq poids (read))
    (format t "~%Quel est votre objectif de poids ?~%")
    (setq objectif (read))
    (setq diff (- poids objectif))
    diff)
  )
      

;;-------- CHAINAGE ARRIERE --------------------

(defun verifier_ou (but bdR &optional (i 0))
  (if (connu? but *BF*) ;; Condition d'arr�t lorsque le but appartient � la base de fait
      (progn 
        ;;(format t "~V@t But : ~A proof ~%" i but)
        T)
    ;;On r�cup�re toutes les r�gles candidates du but
    (let ((regles (reverse (regles-candidates but bdR))) (ok nil))

     (while (and regles (not ok))
       ;;(format t "~% ~V@t VERIFIE_OU ~A Regles ~s :  ~A ~%" i but (numRegle (car regles)) (car regles))
       (setq ok (verifier_et (pop regles) bdR i)))
      
      ;;On v�rifie que le but n'appartient pas � la base de fait (ok = nil)
      ;;et que le but n'est pas une conclusion d'une r�gle (on ne peut pas le d�duire, il est n�cessaire que l'utilisateur renseigne ce fait) 
      (if (and (not ok) (not (conclusion? but bdR)))
          
          ;;On verifie que le fait n'appartient � la base de fait 
          ;;Cela signifie que l'utilisateur n'a encore jamais renseign� une valeur pour ce fait 
          (if (not (assoc (cadr but) *BF*))
              (progn
                
                ;;On met � jour la base de fait avec la reponse de l'utilisateur � une question li�e au but en question
                (setq *BF* (question-utilisateur (cadr but)))
                (format t "~% ~V@t Mise � jour de la base de fait : ~s ~%" i *BF*)
                
                ;;On reefectue le m�me traitement mais avec la nouvelle base de fait mise � jour
                (setq ok (verifier_ou but bdR i)))))      
      ok)))

;;Fonction permettant de traiter chaque pr�misse d'une r�gle pour savoir si le but est connu dans la base de fait 
(defun verifier_et (regle bdR i)
  (let ((ok t) (premisses (termes-conditions regle)))
    (while (and premisses ok)
     ;; (format t "~V@t  ~t VERIFIE_ET ~s premisse ~A~%" (+ 1 i) (numRegle regle) (car premisses))
      (setq ok (verifier_ou (pop premisses) bdR (+ 6 i))))
    ok))

;;Fonction posant une question associ� � un fait ex : temps,niveau,... et mets � jour la r�ponse dans la base de fait
(defun question-utilisateur (but)
  (let(question)
    ;;On r�cup�re l'intitul� de la question en fonction du fait pass� en param�tre
    (setq question (cadr (assoc but BQ)))
    
    ;;Affichage de la question
    (print question)
    (format t "~%")
    ;;Mise � jour de la base avec la r�ponse
    (push (list but (read)) *BF*)
    *BF*))


;;------------- FONCTIONS POUR LE CHAINAGE AVANT ---------------------

;;Cette fonction teste si un fait est connu dans la base de fait (renvoie vrai ou faux)
;;Cette fonction est utile lorsque la base de fait contient des faits poss�dant le m�me attribut mais pas la m�me valeur
;;Exemple de base de fait dans ce cas = ((ENTRAINEMENT musculation)(ENTRAINEMENT remise_en_forme))

(defun connu?BF (fait bdF) ;;Fait de la forme (operateur attribut valeur)
  
  ;;on recup�re tous les faits ayant le m�me attribut que "fait" dans bdF
  (let((valeurs (assoclist2 (cadr fait) bdf)) vrai? valeur)
    (dolist (x valeurs)
      ;;On teste si le fait co�ncide avec l'un des fait de bdF
      (setq valeur (cadr x))
      (if (funcall (car fait) valeur (caddr fait))
          (setq vrai? T)))
    vrai?))

;;Fonction permettant de savoir si les premisses de "regle" sont connues dans bdF 
;;Renvoie T si toutes les pr�misses sont connues dans bdF
;;Renvoie NIL, sinon.
(defun conditions-valides (regle bdF)
  (let ((valide? T)(conditions (termes-conditions regle)))
    
    ;;Tant que la condition est valide et que nous n'avons pas traiter l'ensemble des pr�misses
    (while (and valide? conditions)
      
      ;;Si, la pr�misse n'est pas v�rifi�e, le programme s'arr�te et renvoie nil
      (if (not (connu?BF (car conditions) bdF))
          (setq valide? NIL)
        )
      ;;Si la pr�misse est connue, on passe � la suivante
      (setq conditions (cdr conditions)))
    valide?))

;;Fonction permettant de savoir si une r�gle est d�clenchable dans bdR en fonction des faits pr�sents dans bdF
;;Renvoie T si elle peut �tre d�clench�
;;NIL sinon. Cela signifie que plus aucune r�gle ne peut �tre d�clench�e dans la bdR

(defun ?regles-declenchables (bdR bdF)
  (let(conditions ?declenchables)
    
    ;;Pour chaque regle, on regarde si elle est d�clenchable
    ;;Si ses pr�misses co�ncides avec les faits alors on passe ?declenchables � T
    (dolist (regle bdR)
      (if (conditions-valides regle bdF)
          (setq ?declenchables T)
        )
      )
    ?declenchables))

;;Fonction qui ajoute la conclusion d'une r�gle dans la base de faits 
(defun appliquer-regle (regle)
  (let((conclusion nil)(nvFait nil))
    
    ;;On recup�re la conclusion de la r�gle 
    (setq conclusion (termes-conclusion regle))
    
    ;;On r��crit correctement le fait pour l'ins�rer dans la base de faits
    (setq nvFait (list (cadr conclusion) (caddr conclusion)))
    (pushnew nvFait baseFait)))

;;Fonction supprimant une r�gle de la baseRegles
(defun supprimer-regle (regle)
  (delete regle baseRegles))

;;Fonction qui permet de r�cup�rer tous les faits ayant le m�me attribut dans la base de faits
(defun assoclist2 (attribut bdF)
  
  ;;On recup�re le premier le fait ayant l'attribut pass� en param�tre
  (let((f (assoc attribut bdF)) list)
    ;;Tant qu'il existe un fait ayant l'attribut en question
    (while f
      
      ;;On enleve le fait en question de bdF et on l'ins�re dans une liste
      (setq bdF (remove f bdF :test #'equal))
      (push f list)
      
      ;;on met � jour f avec le fait qui co�ncide par la suite
      (setq f (assoc attribut bdF)))
    
    ;;On retourne la liste contenant tous les faits poss�dant l'attribut pass� en argument
    list))
    

(defun chainage-avant (BF)
  ;;On effectue une copie de BF et de *BR*
  (setq baseFait (copy-list BF))
  (setq baseRegles (copy-list *BR*))
  
  ;;Le traitement s'effectue tant qu'il est possible de d�clencher une r�gle avec les faits
  (while (?regles-declenchables baseRegles baseFait)
    
    ;;Pour chaque r�gle, on regarde si ses pr�misses sont valid�s
    (dolist (regle baseRegles)
      ;;(format t "~% Regle = ~s~%" regle)
      
      ;;Pour chaque r�gle, on regarde si ses pr�misses sont valid�s
      (if (conditions-valides regle baseFait)
          (progn
            
            ;;Si oui, on applique la regle => MAJ de la BF
            (appliquer-regle regle)
            ;;(format t "~% Base de fait : ~s ~%" baseFait)
            
            ;;Puis on supprime la r�gle, on ne peut plus la re-d�clencher
            (supprimer-regle regle)
            ;;(format t "~% Base de regles : ~s ~%" baseRegles)
            ))))
  
  ;;Une fois le chainage termin�, 
  ;;on regarde si des faits ayant comme attribut "entrainement"
  (if (assoclist2 'entrainement baseFait)
      (progn
        (format t "~%Voici les entrainements adapt�s pour vous :~%")
        
        ;;Si oui, on affiche tous les entrainements d�duits par le chainage avant
        (dolist (x (assoclist2 'entrainement baseFait))
          (format t "~% - ~s " (cadr x))
          )
        ;;On renvoie T
        T)
    ;;Sinon, aucun entrainement d�duit
    (progn
      (format t "Aucun entrainement est disponible avec vos informations")
      nil)))
         
    
;;(chainage-avant)               
                      

;;----------- MAIN -------------------

;;TEST 
(setq *BF* nil)

(defun main()
  (let ((choix 0)(poids_initial 0)(poids_obj 0)(instance nil)(freq 0)(temps 0)
        (acces nil)(h nil)(c nil)(b nil)(ent nil)(niv nil)
        resultat_arriere resultat_arriere2 resultat_avant (objectif nil)
        )
    (format t "~%Bienvenue dans le Syst�me Expert qui organise vos entrainements de sport~%")
    (format t "~%1 : Chercher un entrainement adapt� en fonction de mes moyens (chainage avant)~%")
    (format t "2 : Savoir si un entrainement est possible quand je connais mes moyens (chainage arri�re)~%")
    (format t "3 : Savoir si un entrainement est possible quand je connais mes moyens (chainage arri�re)~%")
    (format t "~%Choisissez votre choix (1,2 ou 3)~%")
    (setq choix (read))
    (if (eq choix 1)
        (progn 
          (format t "~%1 : Chercher un entrainement adapt� en fonction de mes moyens (chainage avant)~%")
          (push (list 'delta_poids (ObjectifPoids)) instance)
          (format t "~%Combien de s�ances �tes vous pret � faire par semaine ?~%")
          (setq freq (read))
          (push (list 'frequence_semaine freq) instance)
          (format t "~%Combien de temps disposez vous pour faire chaque s�ance en heure ? (0 <= 30 min, 1 <= 1h, 2 <= 2h, 3 > 2h)~%")
          (setq temps (read))
          (push (list 'temps temps) instance)
          (format t "~%Avez vous acc�s � une salle de sport pour vos entrainements ? (oui ou non)~%")
          (setq acces (read))
          (if (eq acces 'oui)
              (push (list 'acces_sds 'true) instance)
            (progn
              (push (list 'acces_sds 'false) instance)
              (format t "~%Avez vous � disposition des halt�res ? (oui ou non)~%")
              (setq h (read))
              (if (eq h 'oui)
                  (push (list 'halteres 'true) instance)
                (push (list 'halteres 'false) instance)
                )
              (format t "~%Avez vous � disposition une corde � sauter ? (oui ou non)~%")
              (setq c (read))
              (if (eq c 'oui)
                  (push (list 'corde_saute 'true) instance)
                (push (list 'corde_saute 'false) instance)
                )
              (format t "~%Avez vous � disposition une barre de traction ? (oui ou non)~%")
              (setq b (read))
              (if (eq b 'oui)
                  (push (list 'barre_traction 'true) instance)
                (push (list 'barre_traction 'false) instance)
                )))
          (format t "~%Si vous deviez evaluer votre niveau sur une echelle de 0 � 3 (0 : debutant, 3 : expert~%")
          (setq niv (read))
          (push (list 'niveau niv) instance)
          
          (chainage-avant instance)
      ))
    (if (eq choix 2)
        (progn 
          (setq *BF* nil)
          (format t "~%2 : Savoir si un entrainement est possible en fonction de mes moyens (chainage arri�re)~%")
          (push (list 'delta_poids (ObjectifPoids)) *BF*)
          (format t "~%Combien de s�ances �tes vous pret � faire par semaine ?~%")
          (setq freq (read))
          (push (list 'frequence_semaine freq) *BF*)
          (format t "~%Combien de temps disposez vous pour faire chaque s�ance en heure ? (0 <= 30 min, 1 <= 1h, 2 <= 2h, 3 > 2h)~%")
          (setq temps (read))
          (push (list 'temps temps) *BF*)
          (format t "~%Avez vous acc�s � une salle de sport pour vos entrainements ? (oui ou non)~%")
          (setq acces (read))
          (if (eq acces 'oui)
              (push (list 'acces_sds 'true) *BF*)
            (push (list 'acces_sds 'false) *BF*)
            )
          (format t "~%Avez vous � disposition des halt�res ? (oui ou non)~%")
          (setq h (read))
          (if (eq h 'oui)
              (push (list 'halteres 'true) *BF*)
            (push (list 'halteres 'false) *BF*)
            )
          (format t "~%Avez vous � disposition une corde � sauter ? (oui ou non)~%")
          (setq c (read))
          (if (eq c 'oui)
              (push (list 'corde_saute 'true) *BF*)
            (push (list 'corde_saute 'false) *BF*)
            )
          (format t "~%Avez vous � disposition une barre de traction ? (oui ou non)~%")
          (setq b (read))
          (if (eq b 'oui)
              (push (list 'barre_traction 'true) *BF*)
            (push (list 'barre_traction 'false) *BF*)
            )
          (format t "~%Voici vos informations renseign�es dans notre syst�me : ~%")
          (format t "~% > ~s ~%" *BF*)
          (format t "~%Quel entrainement voulez vous faire ? Saisir celui qui vous interesse~%")
          (format t "~% - cardio  ~% - CardioSalle ~% - Insanity ~% - crossfit ~% - tabata ~% - musculation_en_salle ~% - musculation_maison ~% -  remise_en_forme ~% - InsaneAbs ~% - GainageAbs ~% - etirements ~%")     
          (setq ent (read))
          (setq objectif (list 'EQ 'ENTRAINEMENT ent))
          
          
          (setq resultat_arriere (verifier_ou objectif *BR*))
          (if resultat_arriere
              (format t "~%Vous pouvez effectuer cet entrainement ! ")
            (format t "Erreur, cet entrainement n'est pas adapt� pour vous !"))
          (return-from main resultat_arriere)))
    
          ;;(setq resultat_arriere (verifier_ou objectif *BR*))
          ;;(if resultat_arriere
            ;;  (format t "Avec vos informations renseign�es, vous �tes capables d'effectuer cet entrainement : ~s " ent)
            ;;(format t "Erreur, impossible de faire cette entrainement ! "))
          ;;))
    (if (eq choix 3)
        (progn 
          (format t "~%3 : Savoir si un entrainement est possible quand je connais mes moyens (chainage arri�re)~%")
          (setq *BF* nil)
          (format t "~%Quel entrainement voulez vous faire ? Saisir celui qui vous interesse~%")
          (format t "~% - cardio  ~% - CardioSalle ~% - Insanity ~% - crossfit ~% - tabata ~% - musculation_en_salle ~% - musculation_maison ~% - remise_en_forme ~% - InsaneAbs ~% - GainageAbs ~% - etirements ~%")
          (setq ent (read))
          (setq objectif (list 'EQ 'ENTRAINEMENT ent))
          (verifier_ou objectif *BR*)
          ;;(setq resultat_arriere2 (verifier_ou objectif *BR*))
          ;;(if resultat_arriere
             ;;(format t "Avec vos informations renseign�es, vous �tes capables d'effectuer cet entrainement : ~s " ent)
            ;;(format t "Erreur, impossible de faire cette entrainement ! "))
          ))))
 

(main)


